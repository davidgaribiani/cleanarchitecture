﻿using Application.Abstractions.Caching;
using Microsoft.Extensions.Caching.Distributed;
using System.Text.Json;

namespace Infrastructure.Adapters
{
    public class DistributedCachingAdapter : IDistributedCachingAdapter
    {
        private readonly IDistributedCache _distributedCache;

        public DistributedCachingAdapter(IDistributedCache distributedCache)
        {
            _distributedCache = distributedCache;
        }

        public void Add(string key, object value, TimeSpan duration)
        {
            if (value == null)
            {
                throw new ArgumentNullException(nameof(value), "Value cannot be null.");
            }

            var options = new DistributedCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = duration
            };

            var serializedValue = JsonSerializer.Serialize(value);

            _distributedCache.SetString(key, serializedValue, options);
        }

        public void Remove(string key)
        {
            _distributedCache.Remove(key);
        }

        public bool TryGetValue<T>(string key, out T value)
        {
            var serializedValue = _distributedCache.GetString(key);

            if (!string.IsNullOrEmpty(serializedValue))
            {
                value = JsonSerializer.Deserialize<T>(serializedValue);
                return true;
            }

            value = default;
            return false;
        }
    }
}
