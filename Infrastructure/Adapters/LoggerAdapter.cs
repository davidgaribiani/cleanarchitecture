﻿using Application.Abstractions;
using Serilog;
using Serilog.Events;

namespace Infrastructure.Adapters
{
    public class LoggerAdapter : ILoggerAdapter
    {
        private readonly ILogger _logger;

        public LoggerAdapter() => _logger = new LoggerConfiguration()
                .MinimumLevel.Verbose()
                .WriteTo.Console()
                .WriteTo.Logger(l => l.Filter.ByIncludingOnly(e => e.Level == LogEventLevel.Information).WriteTo.File($@"Logs\{DateTime.Now.ToString("yyyy.MM.dd")}\Info.log"))
                .WriteTo.Logger(l => l.Filter.ByIncludingOnly(e => e.Level == LogEventLevel.Debug).WriteTo.File($@"Logs\{DateTime.Now.ToString("yyyy.MM.dd")}\Debug.log"))
                .WriteTo.Logger(l => l.Filter.ByIncludingOnly(e => e.Level == LogEventLevel.Warning).WriteTo.File($@"Logs\{DateTime.Now.ToString("yyyy.MM.dd")}\Warning.log"))
                .WriteTo.Logger(l => l.Filter.ByIncludingOnly(e => e.Level == LogEventLevel.Error).WriteTo.File($@"Logs\{DateTime.Now.ToString("yyyy.MM.dd")}\Error.log"))
                .WriteTo.Logger(l => l.Filter.ByIncludingOnly(e => e.Level == LogEventLevel.Fatal).WriteTo.File($@"Logs\{DateTime.Now.ToString("yyyy.MM.dd")}\Fatal.log"))
                .CreateLogger();

        public void LogInformation(string message) => _logger.Information(message);

        public void LogError(string message, Exception ex) => _logger.Error(ex, message);
    }
}
