﻿using Application.Abstractions.Caching;
using Microsoft.Extensions.Caching.Memory;

namespace Infrastructure.Adapters
{
    public class MemoryCachingAdapter : IMemoryCachingAdapter
    {
        private readonly IMemoryCache _memoryCache;

        public MemoryCachingAdapter(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }

        public void Add(string key, object value, TimeSpan duration)
        {
            // Use the IMemoryCache to add the item with the specified duration
            _memoryCache.Set(key, value, new MemoryCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = duration
            });
        }

        public bool TryGetValue<T>(string key, out T value)
        {
            // Try to get the value from the IMemoryCache
            if (_memoryCache.TryGetValue(key, out value))
            {
                // If the value exists in the IMemoryCache, return true
                return true;
            }

            // If the value doesn't exist in the IMemoryCache, return false
            value = default;
            return false;
        }

        public void Remove(string key)
        {
            // Remove the item from the IMemoryCache
            _memoryCache.Remove(key);
        }
    }
}
