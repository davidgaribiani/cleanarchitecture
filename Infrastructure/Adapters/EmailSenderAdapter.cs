﻿using Application.Abstractions;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Configuration;
using MimeKit;

namespace Infrastructure.Adapters
{
    public class EmailSenderSettings
    {
        public required string SmtpServer { get; set; }
        public int SmtpPort { get; set; } = 587;
        public bool UseSsl { get; set; } = false;
        public required string Username { get; set; }
        public required string Password { get; set; }
    }

    public class EmailSenderAdapter : IEmailSenderAdapter
    {
        private readonly EmailSenderSettings _emailSenderSettings;

        public EmailSenderAdapter(IConfiguration configuration)
        {
            _emailSenderSettings = configuration.GetSection("EmailSenderSettings")?.Get<EmailSenderSettings>()
                ?? throw new InvalidOperationException("EmailSenderSettings configuration is missing or invalid.");
        }


        public bool SendEmail(
            (string name, string address) fromAddress,
            IEnumerable<(string name, string address)> toAddresses,
            IEnumerable<(string name, string address)>? ccAddresses,
            string subject,
            string body,
            bool isHtml = false)
        {
            try
            {
                using (var message = new MimeMessage())
                {
                    message.Body = new TextPart(isHtml ? "html" : "plain") { Text = body };
                    message.From.Add(new MailboxAddress(fromAddress.name, fromAddress.address));
                    message.Subject = subject;

                    foreach (var toAddress in toAddresses)
                    {
                        message.To.Add(new MailboxAddress(toAddress.name, toAddress.address));
                    }

                    if (ccAddresses != null)
                    {
                        foreach (var ccAddress in ccAddresses)
                        {
                            message.Cc.Add(new MailboxAddress(ccAddress.name, ccAddress.address));
                        }
                    }

                    using (var client = new SmtpClient())
                    {
                        client.Connect(_emailSenderSettings.SmtpServer, _emailSenderSettings.SmtpPort, _emailSenderSettings.UseSsl);
                        client.Authenticate(_emailSenderSettings.Username, _emailSenderSettings.Password);
                        client.Send(message);
                        client.Disconnect(true);
                    }
                }

                return true;
            }
            catch
            {
                // Handle the exception (e.g., log the error, throw a custom exception, etc.)
                // For simplicity, let's catch any exception and return false to indicate failure.
                return false;
            }
        }
    }
}
