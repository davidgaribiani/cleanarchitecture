﻿using Application.DTOs.Auth;
using Domain.Entities.Auth;
using Infrastructure.Abstractions;
using Infrastructure.Identity;
using Infrastructure.Persistence;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using System.Text;

namespace Infrastructure.Auth
{
    public class JwtProvider : IJwtProvider
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IConfiguration _configuration;
        private readonly ApplicationDbContext _context;
        private readonly TokenValidationParameters _tokenValidationParameters;

        public JwtProvider(UserManager<ApplicationUser> userManager, IConfiguration configuration, ApplicationDbContext context, TokenValidationParameters tokenValidationParameters)
        {
            _userManager = userManager;
            _configuration = configuration;
            _context = context;
            _tokenValidationParameters = tokenValidationParameters;
        }

        public async Task<AuthResultDTO> VerifyAndGenerateTokenAsync(TokenRefreshRequestDTO tokenRequesDTO, CancellationToken cancellationToken)
        {
            var jwtTokenHandler = new JwtSecurityTokenHandler();

            var storedToken = await _context.RefreshTokens.FirstOrDefaultAsync(rt =>
                rt.Token == tokenRequesDTO.RefreshToken);

            var dbUser = await _userManager.FindByIdAsync(storedToken.UserId.ToString());

            try
            {
                var tokenCheckResult = await jwtTokenHandler.ValidateTokenAsync(tokenRequesDTO.Token, _tokenValidationParameters);
                return await GenerateJWTTokenAsync(dbUser, storedToken, cancellationToken);
            }
            catch (SecurityTokenExpiredException)
            {
                if (storedToken.DateExpires >= DateTime.UtcNow)
                {
                    return await GenerateJWTTokenAsync(dbUser, storedToken, cancellationToken);
                }
                else
                {
                    return await GenerateJWTTokenAsync(dbUser, null, cancellationToken);
                }
            }
        }

        public async Task<AuthResultDTO> GenerateJWTTokenAsync(ApplicationUser user, RefreshToken refreshToken, CancellationToken cancellationToken)
        {
            var authClaims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.Sub, user.Email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };

            var userClaims = await _userManager.GetClaimsAsync(user);

            foreach (var claim in userClaims.Where(x => x.Type == ClaimTypes.Role))
                authClaims.Add(new Claim(claim.Type, claim.Value));

            var authSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_configuration["Authentication:JWT:Secret"] ?? throw new NullReferenceException()));

            var token = new JwtSecurityToken(
                    issuer: _configuration["Authentication:JWT:Issuer"] ?? throw new NullReferenceException(),
                    audience: _configuration["Authentication:JWT:Audience"] ?? throw new NullReferenceException(),
                    expires: DateTime.UtcNow.AddMinutes(2),
                    claims: authClaims,
                    signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256));

            var jwtToken = new JwtSecurityTokenHandler().WriteToken(token);


            var result = new AuthResultDTO()
            {
                StatusCode = HttpStatusCode.OK,
                Message = "New Jwt Token Generated.",
                JwtToken = jwtToken,
                ExpiresAt = token.ValidTo
            };

            if (refreshToken != null)
            {
                result.RefreshToken = refreshToken.Token;
            }
            else
            {
                var newRefreshToken = new RefreshToken()
                {
                    JwtId = token.Id,
                    IsRevoked = false,
                    UserId = user.Id,
                    DateAdded = DateTime.UtcNow,
                    DateExpires = DateTime.UtcNow.AddMonths(6),
                    Token = Guid.NewGuid().ToString() + "-" + Guid.NewGuid().ToString()
                };

                await _context.RefreshTokens.AddAsync(newRefreshToken, cancellationToken);
                await _context.SaveChangesAsync(cancellationToken);

                result.Message = "New Jwt Token Generated. New Refresh Token generated.";
                result.RefreshToken = newRefreshToken.Token;
            }

            return result;
        }


    }

}
