﻿using Application.Abstractions.Auth;
using Application.DTOs.Auth;
using Infrastructure.Abstractions;
using Infrastructure.Identity;
using Microsoft.AspNetCore.Identity;
using System.Net;
using System.Security.Claims;

namespace Infrastructure.Auth
{
    public class AuthService : IAuthService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IJwtProvider _jwtProvider;

        public AuthService(IJwtProvider jwtProvider, UserManager<ApplicationUser> userManager)
        {
            _jwtProvider = jwtProvider;
            _userManager = userManager;
        }

        public async Task<AuthResultDTO> SignInDefault(SignInRequestDTO signInDTO, CancellationToken cancellationToken)
        {
            var userExists = await _userManager.FindByNameAsync(signInDTO.UserName);

            var result = userExists != null && await _userManager.CheckPasswordAsync(userExists, signInDTO.Password) ?
                await _jwtProvider.GenerateJWTTokenAsync(userExists, null!, cancellationToken) :
                new AuthResultDTO(HttpStatusCode.Unauthorized, "Invalid Credentials.");

            return result;
        }

        public async Task<AuthResultDTO> SignUpDefault(SignUpRequestDTO signUpDTO, CancellationToken cancellationToken)
        {
            var newUser = new ApplicationUser()
            {
                Email = signUpDTO.Email,
                UserName = signUpDTO.UserName,
                SecurityStamp = Guid.NewGuid().ToString()
            };

            var identityResult = await _userManager.CreateAsync(newUser, signUpDTO.Password);

            var result = identityResult.Succeeded ?
                new AuthResultDTO(HttpStatusCode.Created, "User created successfully") :
                new AuthResultDTO(HttpStatusCode.BadRequest, "User was not created");

            if (identityResult.Succeeded)
            {
                await _userManager.AddClaimAsync(newUser, new Claim(ClaimTypes.Role, "User"));
            }

            return result;
        }

        public async Task<AuthResultDTO> RefreshToken(TokenRefreshRequestDTO tokenRequestDTO, CancellationToken cancellationToken)
        {
            var result = await _jwtProvider.VerifyAndGenerateTokenAsync(tokenRequestDTO, cancellationToken);
            return result;
        }
    }
}
