﻿using Application.DTOs.Auth;
using Domain.Entities.Auth;
using Infrastructure.Identity;

namespace Infrastructure.Abstractions
{
    public interface IJwtProvider
    {
        Task<AuthResultDTO> VerifyAndGenerateTokenAsync(TokenRefreshRequestDTO tokenRequesDTO, CancellationToken cancellationToken);
        Task<AuthResultDTO> GenerateJWTTokenAsync(ApplicationUser user, RefreshToken refreshToken, CancellationToken cancellationToken);
    }
}
