﻿using Application.Abstractions;
using Application.Abstractions.Repositories;
using Application.Common;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Persistence.Repositories
{
    public class ItemRepository : IItemRepository
    {
        private readonly IApplicationDbContext _context;

        public ItemRepository(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task AddAsync(Item entityToAdd, CancellationToken cancellationToken)
        {
            await _context.Items.AddAsync(entityToAdd, cancellationToken);
        }

        public async Task<PagedResult<Item>> GetAllAsyncPagedAsNoTracking(int pageNumber, int pageSize, CancellationToken cancellationToken)
        {

            IQueryable<Item> query = _context.Items;

            int totalCount = await query.CountAsync(cancellationToken);

            List<Item> items = await query
                    .Skip((pageNumber - 1) * pageSize)
                    .Take(pageSize)
                    .AsNoTracking()
                    .ToListAsync(cancellationToken);

            PagedResult<Item> result = new(true, items, null!, pageNumber, pageSize, totalCount);

            return result;
        }

        public async Task<IEnumerable<Item>> GetAllAsyncAsNoTracking(CancellationToken cancellationToken)
        {
            var result = await _context.Items.AsNoTracking().ToListAsync(cancellationToken);
            return result;
        }
    }
}
