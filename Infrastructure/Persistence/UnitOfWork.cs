﻿using Application.Abstractions;
using Application.Abstractions.Repositories;
using Infrastructure.Persistence.Repositories;

namespace Infrastructure.Persistence
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private bool _disposed = false;
        private readonly ApplicationDbContext _context;
        private IItemRepository _itemRepository = null!;

        public UnitOfWork(ApplicationDbContext context) => _context = context;

        public IItemRepository ItemRepository => _itemRepository ??= new ItemRepository(_context);

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken) => await _context.SaveChangesAsync(cancellationToken);

        public async Task DisposeAsync() => await _context.DisposeAsync();

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }

                _disposed = true;
            }
        }
    }
}
