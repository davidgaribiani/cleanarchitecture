﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Entities.Auth
{
    public class RefreshToken
    {
        [Key]
        public string Token { get; set; }
        public string JwtId { get; set; }
        public bool IsRevoked { get; set; }
        public Guid UserId { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime DateExpires { get; set; }
    }
}
