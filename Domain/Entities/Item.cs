﻿using Domain.Common;

namespace Domain.Entities
{
    public class Item : BaseEntity
    {
        public string Value { get; set; }

        public Item(string value)
        {
            Value = value;
        }
    }
}
