﻿using Application.Abstractions;
using Application.Abstractions.Services;
using Application.DTOs;
using Domain.Entities;

namespace Application.Services
{
    public class ItemStorageService : IItemStorageService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ItemStorageService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Guid> AddItemAsync(ItemDTO dto, CancellationToken cancellationToken)
        {
            var entityToAdd = new Item(dto.Value);
            await _unitOfWork.ItemRepository.AddAsync(entityToAdd, cancellationToken);
            await _unitOfWork.SaveChangesAsync(cancellationToken);
            return entityToAdd.Id;
        }

        public async Task<IEnumerable<ItemDTO>> GetItemsAsync(CancellationToken cancellationToken)
        {
            var items = await _unitOfWork.ItemRepository.GetAllAsyncAsNoTracking(cancellationToken);
            var res = items.Select(x => new ItemDTO(x.Id, x.Value));
            return res;
        }
    }
}
