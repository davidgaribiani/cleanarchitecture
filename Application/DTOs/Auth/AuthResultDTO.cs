﻿using System.Net;

namespace Application.DTOs.Auth
{
    public class AuthResultDTO
    {
        public AuthResultDTO()
        {
        }

        public AuthResultDTO(HttpStatusCode statusCode, string message)
        {
            StatusCode = statusCode;
            Message = message;
        }

        public HttpStatusCode StatusCode { get; set; }
        public string Message { get; set; }
        public string JwtToken { get; set; }
        public string RefreshToken { get; set; }
        public DateTime ExpiresAt { get; set; }
    }
}
