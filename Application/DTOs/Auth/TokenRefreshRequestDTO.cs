﻿namespace Application.DTOs.Auth
{
    public class TokenRefreshRequestDTO
    {
        public string Token { get; set; } = string.Empty;
        public string RefreshToken { get; set; } = string.Empty;
    }
}
