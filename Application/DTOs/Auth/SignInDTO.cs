﻿namespace Application.DTOs.Auth
{
    public class SignInRequestDTO
    {
        public SignInRequestDTO()
        {
        }

        public SignInRequestDTO(string userName, string password)
        {
            UserName = userName;
            Password = password;
        }

        public string UserName { get; set; } = string.Empty;
        public string Password { get; set; } = string.Empty;
    }
}
