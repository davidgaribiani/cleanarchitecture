﻿namespace Application.DTOs.Auth
{
    public class SignUpRequestDTO
    {
        public SignUpRequestDTO()
        {
        }

        public SignUpRequestDTO(string userName, string email, string password)
        {
            UserName = userName;
            Email = email;
            Password = password;
        }

        public string UserName { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;
        public string Password { get; set; } = string.Empty;

    }
}
