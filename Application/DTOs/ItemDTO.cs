﻿namespace Application.DTOs
{
    public class ItemDTO
    {
        public Guid Id { get; set; }
        public string Value { get; set; }

        public ItemDTO(string value)
        {
            Value = value;
        }

        public ItemDTO(Guid id, string value)
        {
            Id = id;
            Value = value;
        }
    }
}
