﻿namespace Application.Abstractions
{
    public interface IEmailSenderAdapter
    {
        /// <summary>
        /// Sends an email using the provided details.
        /// </summary>
        /// <param name="fromAddress">The sender's name and email address.</param>
        /// <param name="toAddresses">A collection of recipient names and email addresses.</param>
        /// <param name="ccAddresses">A collection of CC (Carbon Copy) recipient names and email addresses. Can be null if there are no CC recipients.</param>
        /// <param name="subject">The subject of the email.</param>
        /// <param name="body">The body content of the email.</param>
        /// <param name="isHtml">Indicates whether the email body contains HTML content (default is false for plain text).</param>
        /// <returns>Returns true if the email is sent successfully; otherwise, returns false.</returns>
        bool SendEmail(
            (string name, string address) fromAddress,
            IEnumerable<(string name, string address)> toAddresses,
            IEnumerable<(string name, string address)>? ccAddresses,
            string subject,
            string body,
            bool isHtml = false
        );
    }
}
