﻿using Application.DTOs;

namespace Application.Abstractions.Services
{
    public interface IItemStorageService
    {
        Task<Guid> AddItemAsync(ItemDTO dto, CancellationToken cancellationToken);
        Task<IEnumerable<ItemDTO>> GetItemsAsync(CancellationToken cancellationToken);
    }
}
