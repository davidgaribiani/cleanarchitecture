﻿using Domain.Entities;
using Domain.Entities.Auth;
using Microsoft.EntityFrameworkCore;


namespace Application.Abstractions
{
    public interface IApplicationDbContext
    {
        DbSet<Item> Items { get; set; }
        DbSet<RefreshToken> RefreshTokens { get; set; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
