﻿namespace Application.Abstractions.Caching
{
    public interface ICachingAdapter
    {
        void Add(string key, object value, TimeSpan duration);
        bool TryGetValue<T>(string key, out T value);
        void Remove(string key);
    }
}
