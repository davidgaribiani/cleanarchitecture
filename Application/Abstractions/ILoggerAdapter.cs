﻿namespace Application.Abstractions
{
    public interface ILoggerAdapter
    {
        void LogInformation(string message);
        void LogError(string message, Exception ex);
    }
}
