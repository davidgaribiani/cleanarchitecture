﻿using Application.Abstractions.Repositories;

namespace Application.Abstractions
{
    public interface IUnitOfWork
    {
        IItemRepository ItemRepository { get; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
        Task DisposeAsync();
    }
}
