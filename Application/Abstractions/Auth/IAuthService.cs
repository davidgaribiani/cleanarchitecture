﻿using Application.DTOs.Auth;

namespace Application.Abstractions.Auth
{
    public interface IAuthService
    {
        Task<AuthResultDTO> SignInDefault(SignInRequestDTO signInDTO, CancellationToken cancellationToken);
        Task<AuthResultDTO> SignUpDefault(SignUpRequestDTO signUpDTO, CancellationToken cancellationToken);
        Task<AuthResultDTO> RefreshToken(TokenRefreshRequestDTO tokenRequestDTO, CancellationToken cancellationToken);
    }
}
