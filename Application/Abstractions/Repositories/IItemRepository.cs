﻿using Application.Common;
using Domain.Entities;

namespace Application.Abstractions.Repositories
{
    public interface IItemRepository
    {
        Task AddAsync(Item entityToAdd, CancellationToken cancellationToken);
        Task<IEnumerable<Item>> GetAllAsyncAsNoTracking(CancellationToken cancellationToken);
        Task<PagedResult<Item>> GetAllAsyncPagedAsNoTracking(int pageNumber, int pageSize, CancellationToken cancellationToken);
    }
}
