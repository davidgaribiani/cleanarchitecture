﻿namespace Application.Common
{
    public class PagedResult<T> : Result<IEnumerable<T>>
    {
        public int PageNumber { get; }
        public int PageSize { get; }
        public int TotalCount { get; }

        public PagedResult(bool success, IEnumerable<T> value, string errorMessage, int pageNumber, int pageSize, int totalCount)
            : base(success, value, errorMessage)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            TotalCount = totalCount;
        }

        public static PagedResult<T> Success(IEnumerable<T> value, int pageNumber, int pageSize, int totalCount)
        {
            return new PagedResult<T>(true, value, null, pageNumber, pageSize, totalCount);
        }

        public static PagedResult<T> Failure(string errorMessage)
        {
            return new PagedResult<T>(false, default(IEnumerable<T>), errorMessage, 0, 0, 0);
        }
    }
}