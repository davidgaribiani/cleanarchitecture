﻿using Domain.Entities;
using MediatR;

namespace Application.Queries
{
    public record GetItemsQuery(int PageNumber, int PageSize) : IRequest<IEnumerable<Item>>;
}
