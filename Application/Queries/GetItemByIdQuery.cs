﻿using Application.Common;
using Application.DTOs;
using MediatR;


namespace Application.Queries
{
    public record GetItemByIdQuery(Guid Id) : IRequest<Result<ItemDTO>>;
}
