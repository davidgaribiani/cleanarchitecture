﻿using Application.Abstractions;
using Domain.Entities;
using MediatR;

namespace Application.Queries
{
    public class GetItemsQueryHandler : IRequestHandler<GetItemsQuery, IEnumerable<Item>>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetItemsQueryHandler(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task<IEnumerable<Item>> Handle(GetItemsQuery request, CancellationToken cancellationToken)
        {
            var result = await _unitOfWork.ItemRepository.GetAllAsyncAsNoTracking(cancellationToken);
            return result;
        }
    }
}
