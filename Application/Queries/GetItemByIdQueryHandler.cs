﻿using Application.Abstractions;
using Application.Common;
using Application.DTOs;
using AutoMapper;
using MediatR;

namespace Application.Queries
{
    public class GetItemByIdQueryHandler : IRequestHandler<GetItemByIdQuery, Result<ItemDTO>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetItemByIdQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<Result<ItemDTO>> Handle(GetItemByIdQuery request, CancellationToken cancellationToken)
        {
            var item = await _context.Items.FindAsync(request.Id);
            var dto = _mapper.Map<ItemDTO>(item);
            var result = Result<ItemDTO>.Success(dto);
            return result;
        }
    }
}
