﻿using Domain.Entities;
using MediatR;

namespace Application.Commands
{
    public record AddItemCommand(Item Item) : IRequest<Item>;
}
