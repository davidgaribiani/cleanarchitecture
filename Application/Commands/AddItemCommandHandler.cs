﻿using Application.Abstractions;
using Domain.Entities;
using MediatR;

namespace Application.Commands
{
    public class AddItemCommandHandler : IRequestHandler<AddItemCommand, Item>
    {
        private readonly IUnitOfWork _unitOfWork;

        public AddItemCommandHandler(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task<Item> Handle(AddItemCommand request, CancellationToken cancellationToken)
        {
            await _unitOfWork.ItemRepository.AddAsync(request.Item, cancellationToken);
            await _unitOfWork.SaveChangesAsync(cancellationToken);
            return request.Item;
        }
    }
}
