﻿using Application.DTOs.Auth;
using AutoMapper;
using WebApi.ViewModels;

namespace WebApi
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<SignInRequestVM, SignInRequestDTO>().ReverseMap();
            CreateMap<SignUpRequestVM, SignUpRequestDTO>().ReverseMap();
            CreateMap<TokenRefreshRequestVM, TokenRefreshRequestDTO>().ReverseMap();
            CreateMap<AuthResultVM, AuthResultDTO>().ReverseMap();
        }
    }
}
