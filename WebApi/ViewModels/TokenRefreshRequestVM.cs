﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.ViewModels
{
    public class TokenRefreshRequestVM
    {
        [Required]
        public string Token { get; set; }
        [Required]
        public string RefreshToken { get; set; }
    }
}
