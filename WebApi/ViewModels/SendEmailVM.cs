﻿using Application.Abstractions;

namespace WebApi.ViewModels
{
    public class SendEmailVM
    {
        public (string name, string address) FromAddress { get; set; }
        public required IEnumerable<(string name, string address)> ToAddresses { get; set; }
        public IEnumerable<(string name, string address)>? CcAddresses { get; set; }
        public string Subject { get; set; } = string.Empty;
        public required string Body { get; set; }
        public bool IsHtml { get; set; } = false;
    }
}
