﻿using System.Net;
using System.Text.Json.Serialization;

namespace WebApi.ViewModels
{
    public class AuthResultVM
    {
        public AuthResultVM()
        {
        }

        public AuthResultVM(HttpStatusCode statusCode, string message, string jwtToken, string refreshToken, DateTime expiresAt)
        {
            StatusCode = statusCode;
            Message = message;
            JwtToken = jwtToken;
            RefreshToken = refreshToken;
            ExpiresAt = expiresAt;
        }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public HttpStatusCode StatusCode { get; set; }
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public string Message { get; set; }
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public string JwtToken { get; set; }
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public string RefreshToken { get; set; }
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public DateTime ExpiresAt { get; set; }
    }
}
