﻿namespace WebApi.ViewModels
{
    public class AddItemRequestVM
    {
        public string Value { get; set; }

        public AddItemRequestVM(string value)
        {
            Value = value;
        }
    }
}
