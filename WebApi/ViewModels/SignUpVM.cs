﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.ViewModels
{
    public class SignUpRequestVM
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
