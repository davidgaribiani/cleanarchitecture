﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.ViewModels
{
    public enum SignInMethod
    {
        Default,
        Google,
        Facebook,
        Twitter
    }

    public class SignInRequestVM
    {
        public SignInMethod SignInMethod { get; set; } = SignInMethod.Default;

        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
