﻿using Application.Abstractions;
using Application.Abstractions.Services;
using Application.DTOs;
using Microsoft.AspNetCore.Mvc;
using WebApi.ViewModels;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestsController : ControllerBase
    {
        private readonly ILoggerAdapter _loggerAdapter;

        public TestsController(ILoggerAdapter loggerAdapter)
        {
            _loggerAdapter = loggerAdapter;
        }

        [HttpPost("items")]
        public async Task<IActionResult> AddItem([FromBody] AddItemRequestVM vm, [FromServices] IItemStorageService itemStorageService, CancellationToken cancellationToken)
        {
            var dto = new ItemDTO(vm.Value);
            var res = await itemStorageService.AddItemAsync(dto, cancellationToken);
            return Ok(res);
        }

        [HttpGet("items")]
        public async Task<IActionResult> GetItems([FromServices] IItemStorageService itemStorageService, CancellationToken cancellationToken)
        {
            var res = await itemStorageService.GetItemsAsync(cancellationToken);
            return Ok(res);
        }

        [HttpPost("throw")]
        public async ValueTask<IActionResult> Throw(CancellationToken cancellationToken)
        {
            throw new Exception();
        }

        [HttpPost("log-inf")]
        public async ValueTask<IActionResult> LogDebug(CancellationToken cancellationToken)
        {
            _loggerAdapter.LogInformation("Hello World");
            return Ok();
        }

        [HttpPost("send-email")]
        public void SendEmail(
            [FromBody] SendEmailVM vm,
            [FromServices] IEmailSenderAdapter emailSender,
            CancellationToken cancellationToken)
        {
            emailSender.SendEmail(
                toAddresses: vm.ToAddresses, 
                fromAddress: vm.FromAddress, 
                ccAddresses: vm.CcAddresses,
                subject: vm.Subject,
                body: vm.Body,
                isHtml: vm.IsHtml                
            );
        }
    }
}
