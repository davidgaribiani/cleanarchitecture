﻿using Application.Commands;
using Application.Queries;
using Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ItemsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public ItemsController(IMediator mediator) => _mediator = mediator;

        [HttpGet]
        public async Task<ActionResult> GetAllItems(CancellationToken cancellationToken)
        {
            var result = await _mediator.Send(new GetItemsQuery(0, 0));
            return Ok(result);
        }

        //[HttpGet]
        //public async Task<ActionResult> GetAllItemsPaged(int pageNumber, int pageSize, CancellationToken cancellationToken)
        //{
        //    var result = await _mediator.Send(new GetItemsQuery(pageNumber, pageSize));
        //    return Ok(result);
        //}

        [HttpPost]
        public async Task<ActionResult> AddItem([FromBody] Item item, CancellationToken cancellationToken)
        {
            var result = await _mediator.Send(new AddItemCommand(item));
            //_mediator.
            return Ok(result);
        }

    }
}
