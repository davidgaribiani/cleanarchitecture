﻿using Application.Abstractions.Auth;
using Application.DTOs.Auth;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WebApi.ViewModels;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;
        private readonly IMapper _mapper;

        public AuthController(IAuthService authService, IMapper mapper)
        {
            _authService = authService;
            _mapper = mapper;
        }

        [HttpPost("signin")]
        public async Task<IActionResult> SignIn([FromBody] SignInRequestVM requestVM, CancellationToken cancellationToken)
        {
            return requestVM.SignInMethod switch
            {
                SignInMethod.Default => await SignInDefault(requestVM, cancellationToken),
                _ => BadRequest("Invalid sign in method")
            };
        }


        [HttpPost("signup")]
        public async Task<IActionResult> SignUp([FromBody] SignUpRequestVM requestVM, CancellationToken cancellationToken)
        {
            var dto = _mapper.Map<SignUpRequestDTO>(requestVM);
            var result = await _authService.SignUpDefault(dto, cancellationToken);
            return StatusCode((int)result.StatusCode, _mapper.Map<AuthResultVM>(result));
        }

        [HttpPost("refresh-token")]
        public async Task<IActionResult> RefreshTokne([FromBody] TokenRefreshRequestVM requestVM, CancellationToken cancellationToken)
        {
            var dto = _mapper.Map<TokenRefreshRequestDTO>(requestVM);
            var result = await _authService.RefreshToken(dto, cancellationToken);
            return StatusCode((int)result.StatusCode, _mapper.Map<AuthResultVM>(result));
        }

        // Helper Methods
        private async Task<IActionResult> SignInDefault(SignInRequestVM requestVM, CancellationToken cancellationToken)
        {
            var requestDTO = _mapper.Map<SignInRequestDTO>(requestVM);
            var result = await _authService.SignInDefault(requestDTO, cancellationToken);

            HttpContext.Response.Cookies.Append("jwt-token", result.JwtToken, new CookieOptions()
            {
                //Expires = DateTime.UtcNow.AddDays(Convert.ToInt32(_configuration["Jwt:RefreshTokenExpirationDays"]))
                //ExpiresUtc = DateTime.UtcNow.AddMinutes(Convert.ToInt32(_configuration["Jwt:AccessTokenExpirationMinutes"]))
            });

            HttpContext.Response.Cookies.Append("refresh-token", result.RefreshToken, new CookieOptions()
            {
                HttpOnly = true,
                Secure = true
                //Expires = DateTime.UtcNow.AddDays(Convert.ToInt32(_configuration["Jwt:RefreshTokenExpirationDays"]))
            });

            return StatusCode(((int)result.StatusCode), _mapper.Map<AuthResultVM>(result));
        }
    }
}
