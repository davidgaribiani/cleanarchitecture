﻿using Application.Abstractions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace WebApi.Filters
{
    public class ExceptionFilter : IExceptionFilter
    {
        private readonly ILoggerAdapter _loggerAdapter;

        public ExceptionFilter(ILoggerAdapter loggerAdapter)
        {
            _loggerAdapter = loggerAdapter;
        }

        public void OnException(ExceptionContext context)
        {
            _loggerAdapter.LogError(context.Exception.Message, context.Exception);
            context.Result = new StatusCodeResult(500);
        }
    }
}
