﻿namespace WebApi.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class RefreshTokenRequiredAttribute : Attribute
    {
        public bool IsRequired { get; init; }

        public RefreshTokenRequiredAttribute(bool isRequired = true)
        {
            IsRequired = isRequired;
        }
    }
}
