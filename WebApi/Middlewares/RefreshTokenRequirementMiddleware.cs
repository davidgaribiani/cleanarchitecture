﻿using System.Net;
using WebApi.Attributes;

namespace WebApi.Middlewares;

public class RefreshTokenRequirementMiddleware
{
    private readonly RequestDelegate _next;

    public RefreshTokenRequirementMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task Invoke(HttpContext context)
    {
        bool requiresRefreshToken = IsRefreshTokenRequired(context);

        if (requiresRefreshToken)
        {
            bool hasRefreshToken = !string.IsNullOrWhiteSpace(context.Request.Cookies["RefreshToken"]);

            if (!hasRefreshToken)
            {
                // Return an error response indicating that the refresh token is required
                context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                await context.Response.WriteAsync("Refresh token is required to access this resource.");
                return;
            }
        }

        // Call the next middleware in the pipeline
        await _next(context);
    }

    private bool IsRefreshTokenRequired(HttpContext context)
    {
        var endpoint = context.GetEndpoint();

        if (endpoint != null)
        {
            var attribute = endpoint.Metadata.GetMetadata<RefreshTokenRequiredAttribute>();
            return attribute?.IsRequired ?? false;
        }

        return false;
    }

}
